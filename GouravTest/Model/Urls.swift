



import Foundation
struct Urls : Codable {
	let url : String?
	let expanded_url : String?
	let display_url : String?
	let indices : [Int]?

	enum CodingKeys: String, CodingKey {

		case url = "url"
		case expanded_url = "expanded_url"
		case display_url = "display_url"
		case indices = "indices"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		expanded_url = try values.decodeIfPresent(String.self, forKey: .expanded_url)
		display_url = try values.decodeIfPresent(String.self, forKey: .display_url)
		indices = try values.decodeIfPresent([Int].self, forKey: .indices)
	}

}
