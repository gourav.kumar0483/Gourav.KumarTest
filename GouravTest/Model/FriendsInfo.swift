//
//  FriendsInfo.swift
//  GouravTest
//
//  Created by Gaurav on 31/07/21.
//

import Foundation

struct FriendsInfo : Codable {
  
    let previous_cursor : Int?
    let ids : [Int]?
    let previous_cursor_str : String?
    let next_cursor : Int?
    let next_cursor_str : String?

    enum CodingKeys: String, CodingKey {

        case previous_cursor = "previous_cursor"
        case ids = "ids"
        case previous_cursor_str = "previous_cursor_str"
        case next_cursor = "next_cursor"
        case next_cursor_str = "next_cursor_str"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        previous_cursor = try values.decodeIfPresent(Int.self, forKey: .previous_cursor)
        ids = try values.decodeIfPresent([Int].self, forKey: .ids)
        previous_cursor_str = try values.decodeIfPresent(String.self, forKey: .previous_cursor_str)
        next_cursor = try values.decodeIfPresent(Int.self, forKey: .next_cursor)
        next_cursor_str = try values.decodeIfPresent(String.self, forKey: .next_cursor_str)
    }
}
