//
//  HomeVC.swift
//  GouravTest
//
//  Created by Gaurav on 31/07/21.
//

import UIKit
import TwitterKit

class HomeVC: UIViewController {
    
    
    @IBOutlet weak var imgProfilePhoto: UIImageView!
    @IBOutlet weak var txtUserName: UILabel!
    @IBOutlet weak var txtUserEmail: UILabel!
    @IBOutlet weak var txtUserFollowers: UILabel!
    @IBOutlet weak var txtUserFollowing: UILabel!
    
    @IBOutlet weak var tblList: UITableView!
    
    
    fileprivate var sessionStore: TWTRSessionStore?
    
    private var friends : FriendsInfo?
    private var arrData : Data?
    
    //MARK:- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sessionStore=TWTRTwitter.sharedInstance().sessionStore
        
        loadUser()
        getUserEmail()
        
        if let sessionStore = self.sessionStore{
            if let session=sessionStore.session(){
                
                
                DispatchQueue.global(qos: .background).async { [self] in
                    
                    sendTwitterReqwest(with: "https://api.twitter.com/1.1/friends/ids.json", params: ["id":session.userID], withMethod: "GET")
              
                
                                getTwitterUserReqwest(with: "https://api.twitter.com/1.1/users/show.json", params: ["id":session.userID], withMethod: "GET")
                   
            
                }
                
            }
            
            
            self.tblList.register(UINib(nibName: "FriendCell", bundle: nil), forCellReuseIdentifier: "FriendCell")
         
            self.tblList.tableFooterView = UIView()


            
        }
    }
    
    
    //MARK:- Functions
    
    
    
    
    private func loadUser(){
        
        
        let client = TWTRAPIClient.withCurrentUser()
        if let sessionStore = self.sessionStore{
            if let session=sessionStore.session(){
                
                client.loadUser(withID: session.userID) { [self] (user, error) in
                    
                    guard let  imgProfileUrl = URL(string: user!.profileImageLargeURL) else{
                        return
                    }
                    
                    downloadImage(from: imgProfileUrl)
                    self.txtUserName.text=user?.name ?? "Not available"
                    
                    
                }
            }
        }
        
    }
    
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.async() { [weak self] in
                self?.imgProfilePhoto.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    private func goToLoginVc(){
        let goToLoginVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(goToLoginVc, animated: true)
    }
    
    func getUserEmail(){
        
        
        let client = TWTRAPIClient.withCurrentUser()
        if let sessionStore = self.sessionStore{
            if let session=sessionStore.session(){
                
                
                client.requestEmail { email, error in
                    
                    if (email != nil) {
                        
                        
                        let recivedEmailID = email ?? ""   // received email
                        
                        self.txtUserEmail.text=recivedEmailID
                        
                        
                    }else {
                        UIAlertController.showAlert(with: error, on: self, errorDescription: "email not found")
                        
                        print("error: \(String(describing: error?.localizedDescription))")
                        
                    }
                }
                
            }
        }
    }
    
    
    private func sendTwitterReqwest(with Api:String,params:[String:Any], withMethod:String){
        
        
        let client = TWTRAPIClient.withCurrentUser()
        
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: withMethod, urlString: Api, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(connectionError)")
            }
            
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                print("json: \(json)")
                
                
                let jsonDecoder = JSONDecoder()
                
                let responseModel = try jsonDecoder.decode(FriendsInfo?.self, from: data!)
                
                
                DispatchQueue.main.async{
                    
                    self.txtUserFollowing.text=String((responseModel?.ids!.count)!)
                    
//                    self.tblList.reloadData()
                    
                }
                
            }
            
            
            catch let jsonError as NSError {
                print("json error: \(jsonError.localizedDescription)")
            }
        }
    }
    
    
    private func sendTwitterfriendsReqwest(with Api:String,params:[String:Any], withMethod:String){


        let client = TWTRAPIClient.withCurrentUser()

        var clientError : NSError?

        let request = client.urlRequest(withMethod: withMethod, urlString: Api, parameters: params, error: &clientError)

        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(connectionError)")
            }


            do {

                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                print("json: \(json)")


              
                DispatchQueue.main.async{

              
//                    self.tblList.reloadData()

                }

            }


            catch let jsonError as NSError {
                print("json error: \(jsonError.localizedDescription)")
            }
        }
    }
    
    private func getTwitterUserReqwest(with Api:String,params:[String:Any], withMethod:String){
        
        
        let client = TWTRAPIClient.withCurrentUser()
        
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: withMethod, urlString: Api, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(connectionError)")
            }
            
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                print("json: \(json)")
                
                
                let jsonDecoder = JSONDecoder()
                
                let responseModel = try jsonDecoder.decode(UserTwitter?.self, from: data!)
                
                
                DispatchQueue.main.async{
                    
//                    self.tblList.reloadData()
                }
                
            }
            
            catch let jsonError as NSError {
                print("json error: \(jsonError.localizedDescription)")
            }
        }
        
    }
    
    //MARK:- IBAction
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        
        print("logout")
        
        if let sessionStore = self.sessionStore{
            if let session=sessionStore.session(){
                
                TWTRTwitter.sharedInstance().sessionStore.logOutUserID(session.userID)
                self.goToLoginVc()
                
            }
        }
    }
}




extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tblList.dequeueReusableCell(withIdentifier:"FriendCell",for:indexPath)
        
        
        return cell
        
    }
    
}
