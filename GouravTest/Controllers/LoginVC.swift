//
//  LoginVC.swift
//  GouravTest
//
//  Created by Gaurav on 31/07/21.
//

import UIKit
import TwitterKit


class LoginVC: UIViewController {
    
    
    @IBOutlet weak var btnLogin: ExtButton!
    
    private var sessionStore: TWTRSessionStore?
    
    
    
    //MARK:- lifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLoginButton()
        self.sessionStore = TWTRTwitter.sharedInstance().sessionStore
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    
    //MARK:- Functions
    
    private func setLoginButton(){
        self.btnLogin.backgroundColor=UIColor.blueTwitter
        
    }
    
    
    
    
    private func saveSession(session: TWTRSession) {
        
        TWTRTwitter.sharedInstance().sessionStore.save(session) { (session, error) in
            print(session)
            if let error = error {
                print(error)
            }
        }
    }
    
    private func gotoHome(){
        let goToHome = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(goToHome, animated: true)
    }
    
    private func getLogin(){
        
        TWTRTwitter.sharedInstance().logIn { [self] (session, error) in
            
            if (session != nil) {
                
                saveSession(session: session!)
                gotoHome()
                
                
            }else {
                UIAlertController.showAlert(with: error, on: self, errorDescription: "login failed")
            }
        }
        
    }
    
    private func checkLogin()-> Bool{
        
        if let userInStore = self.sessionStore?.existingUserSessions() {
            
            if ( userInStore.count>0){
                print("User already logged in")
                
                return  true
                
            }
            else{
                return false
            }
        }
        return false
    }
    
    
    
    
    //MARK:- IBAction
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        
        if (checkLogin()){
            
            DispatchQueue.main.async { [self] in
                
                gotoHome()
                
            }
            
        }else{
            getLogin()
            print("No")
        }
        
    }
    
}
