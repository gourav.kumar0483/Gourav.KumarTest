//
//  UIAlertControllerExtensions.swift
//  GouravTest
//
//  Created by Gaurav on 31/07/21.
//

import UIKit

extension UIColor {
    class var blueTwitter: UIColor {
        return UIColor(red: 29.0 / 255.0, green: 161.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
    }
}
