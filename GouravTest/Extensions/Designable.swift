//
//  Designable.swift
//  GouravTest
//
//  Created by Gaurav on 31/07/21.
//


import UIKit


//MARK:- For View

@IBDesignable class ExtView: UIView
{
    @IBInspectable var cornerRadius:CGFloat = 0.0
        {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    @IBInspectable var borderWidth:CGFloat = 0.0
        {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    @IBInspectable var masksToBounds:Bool = true
        {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
    
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
        {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    
    @IBInspectable var twosidedcornerRadius : CGFloat = 0.0
        {
        didSet{
            
            self.clipsToBounds = true
            self.layer.cornerRadius = twosidedcornerRadius
            if #available(iOS 11.0, *) {
                self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            } else {
                // Fallback on earlier versions
            }
            
            // self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            
        }
    }
    
    
    
}



//MARK:- Extension for Button borderColor,borderWidth,CornerRadius etc..
extension UIButton
{
    
    func Blink(){
        self.alpha = 0.0
        UIButton.animate(withDuration: 1.5, animations: {
            self.alpha = 1.0
            }, completion: {
                (value: Bool) in
                self.Blink()
        })

    }
    
    func applyButtoncolorChange()
    {
        self.layer.borderColor = UIColor.lightGray .cgColor
        self.layer.borderWidth = 1
        //        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)
    }
    
    func applygraycolor()
    {
        self.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    func applyButtonChangee()
    {
        self.layer.borderColor = UIColor.lightGray .cgColor
        self.layer.borderWidth = 3
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
}


//MARK:- Extension for Button

@IBDesignable class ExtButton: UIButton
{
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear
        {
        didSet
        {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0
        {
        didSet{
            self.layer.shadowOpacity = Float(shadowOpacity)
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0)
        {
        didSet{
            self.layer.shadowOffset =  shadowOffset
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            self .layer .shadowRadius = CGFloat(shadowRadius)
        }
    }
    
    
    @IBInspectable var cornerRadius:CGFloat = 0
        {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0
        {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}

//MARK:- For TextField


@IBDesignable class ExttxtFld: UITextField
{
    
    @IBInspectable var leftImage: UIImage?
        {
        didSet
        {
            // updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage?
        {
        didSet
        {
            // updateView()
        }
    }
    
    
    
    @IBInspectable var leftPadding: CGFloat = 0
        {
        didSet
        {
             updateView()
        }
    }
    
    
        @IBInspectable var rightPadding: CGFloat = 0
            {
            didSet
            {
                applyPaddingonTextField()
            }
        }
    
    
    @IBInspectable var cornerRadius:CGFloat = 0
        {
        didSet
        {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    @IBInspectable var masksToBounds:Bool = true
     {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
        {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0
        {
        didSet
        {
            self.layer.borderWidth = borderWidth
        }
    }
    
    
    @IBInspectable var leadingImage: UIImage?
        {
        didSet
        {
            // updateView()
        }
    }
    
    
        func applyPaddingonTextField()
        {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            //
            //        self.isEnabled = true
            //        self.borderStyle = UITextBorderStyle.none
            //        self.layer.borderWidth = 1
            //        self.layer.borderColor = UIColor.lightGray.cgColor
            //        self.layer.masksToBounds = true
    
        }
    
        func updateView()
        {
            
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: self.frame.size.height))
            self.leftView = paddingView
            leftViewMode = .always
            
    
        }
}


//MARK:- For TextView

@IBDesignable class ExtTxtView: UITextView {
    
    
        
        @IBInspectable var cornerRadius:CGFloat = 0
            {
            didSet
            {
                self.layer.cornerRadius = cornerRadius
            }
        }
        
        @IBInspectable var borderColor:UIColor = UIColor.clear{
            
            didSet{
                self.layer.borderColor = borderColor.cgColor
                
            }
        }
        
        @IBInspectable var masksToBounds:Bool = true
            {
            
            didSet{
                
                self.layer.masksToBounds = masksToBounds
                
            }
        }
        
        @IBInspectable var shadowColor:UIColor = UIColor.clear{
            
            didSet{
                
                self.layer.shadowColor = shadowColor.cgColor
                
            }
        }
        
        @IBInspectable var shadowOpacity : CGFloat = 0.0
            {
            
            didSet{
                
                self.layer.shadowOpacity = Float(shadowOpacity)
                
            }
        }
        
        @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
            
            didSet{
                
                self.layer.shadowOffset =  shadowOffset
                
            }
        }
        
        @IBInspectable var shadowRadius : CGFloat = 0.0
            {
            didSet{
                
                self .layer .shadowRadius = CGFloat(shadowRadius)
                
            }
        }
        
        @IBInspectable var borderWidth:CGFloat = 0
            {
            didSet
            {
                self.layer.borderWidth = borderWidth
            }
        }
        
        
    override var text: String! { // Ensures that the placeholder text is never returned as the field's text
        get {
            if showingPlaceholder {
                return "" // When showing the placeholder, there's no real text to return
            } else { return super.text }
        }
        set { super.text = newValue }
    }
    @IBInspectable var placeholderText: String = ""
    @IBInspectable var placeholderTextColor: UIColor = UIColor(red: 0.78, green: 0.78, blue: 0.80, alpha: 1.0) // Standard iOS placeholder color (#C7C7CD).
    
    private var showingPlaceholder: Bool = true // Keeps track of whether the field is currently showing a placeholder

    override func didMoveToWindow() {
        super.didMoveToWindow()
        if text.isEmpty {
            showPlaceholderText() // Load up the placeholder text when first appearing, but not if coming back to a view where text was already entered
        }
    }

    override func becomeFirstResponder() -> Bool {
        // If the current text is the placeholder, remove it
        if showingPlaceholder {
            text = nil
            textColor = nil // Put the text back to the default, unmodified color
            showingPlaceholder = false
        }
        return super.becomeFirstResponder()
    }

    override func resignFirstResponder() -> Bool {
        // If there's no text, put the placeholder back
        if text.isEmpty {
            showPlaceholderText()
        }
        return super.resignFirstResponder()
    }

    private func showPlaceholderText() {
        showingPlaceholder = true
        textColor = placeholderTextColor
        text = placeholderText
    }
}



// MARK:- For UILabel

@IBDesignable class ExtLabel: UILabel
{
    @IBInspectable var cornerRadius:CGFloat = 0
        {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0.0
        {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    
    
    
    @IBInspectable var borderColor:UIColor = UIColor.clear
        {
        didSet
        {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    
    
    @IBInspectable var clipsBounds: Bool = true
        {
        didSet
        {
            self.clipsToBounds = clipsBounds
        }
    }
    
    @IBInspectable var masksToBounds:Bool = true
        {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
    @IBInspectable var shadow_Color:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadow_Color.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
        {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadow_Offset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadow_Offset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
}






