//
//  UIAlertControllerExtensions.swift
//  GouravTest
//
//  Created by Gaurav on 31/07/21.
//


import UIKit

extension UIAlertController {
 
    class func showAlert(with error: Error?, on viewController: UIViewController,errorDescription:String?) {
        let alertController = UIAlertController(title: "Alert", message: error?.localizedDescription ?? errorDescription, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel)
        alertController.addAction(dismissAction)
        viewController.present(alertController, animated: true)
    }
}
